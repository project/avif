<?php

namespace Drupal\avif\Controller;

use Drupal\avif\Avif;
use Drupal\image\Controller\ImageStyleDownloadController as CoreImageStyleDownloadController;
use Drupal\image\ImageStyleInterface;
use Drupal\system\FileDownloadController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines a controller to serve image styles.
 */
class ImageStyleDownloadController extends FileDownloadController {

  /**
   * The original image style download controller.
   *
   * @var \Drupal\image\Controller\ImageStyleDownloadController
   */
  protected CoreImageStyleDownloadController $inner;

  /**
   * Avif driver.
   *
   * @var \Drupal\avif\Avif
   */
  protected $avif;

  /**
   * Constructs a ImageStyleDownloadController object.
   *
   * @param \Drupal\image\Controller\ImageStyleDownloadController $inner
   *   The original image style download controller.
   * @param \Drupal\avif\Avif $avif
   *   Avif service.
   */
  public function __construct(CoreImageStyleDownloadController $inner, Avif $avif) {
    $this->inner = $inner;
    $this->avif = $avif;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      CoreImageStyleDownloadController::create($container),
      $container->get('avif.avif'),
    );
  }

  /**
   * Generates a derivative, given a style and image path.
   *
   * After generating an image, transfer it to the requesting agent.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $scheme
   *   The file scheme, defaults to 'private'.
   * @param \Drupal\image\ImageStyleInterface $image_style
   *   The image style to deliver.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
   *   The transferred file as response or some error response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when the user does not have access to the file.
   * @throws \Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException
   *   Thrown when the file is still being generated.
   */
  public function deliver(Request $request, $scheme, ImageStyleInterface $image_style) {
    $target = $request->query->get('file');

    if (!str_ends_with(strtolower($target), '.avif')) {
      return $this->inner->deliver($request, $scheme, $image_style);
    }

    if (!in_array('image/avif', $request->getAcceptableContentTypes())) {
      return new Response($this->t('Error generating image, request does not accept avif format.'), 404);
    }

    $target = substr_replace($target, '', -5);
    $source_request = clone $request;
    $source_request->query->set('file', $target);
    $source_response = $this->inner->deliver($source_request, $scheme, $image_style);

    // Don't try to generate file if source is missing. This also allows the
    // core controller to handle access. If an exception was thrown we won't
    // have a BinaryFileResponse.
    if (!$source_response instanceof BinaryFileResponse) {
      return $source_response;
    }

    $source_file = $source_response->getFile();
    $avif = $this->avif->getAvifCopy($source_file->getPathname());

    if ($avif === FALSE || !$avif->isValid()) {
      return new Response($this->t('Error generating image, unable to generate avif derivative.'), 500);
    }

    $headers = $source_response->headers;
    $headers->set('Content-Type', $avif->getMimeType());
    $headers->set('Content-Length', $avif->getFileSize());

    return new BinaryFileResponse($avif->getSource(), 200, $headers->all(), $scheme !== 'private');
  }

}
