<?php

namespace Drupal\avif\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Avif processor item annotation object.
 *
 * @see \Drupal\avif\Plugin\AvifProcessorManager
 * @see plugin_api
 *
 * @Annotation
 */
class AvifProcessor extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
