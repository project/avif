<?php

namespace Drupal\avif\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * AVIF settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'avif.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'avif_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('avif.settings');

    $form['processor'] = [
      '#type' => 'select',
      '#title' => $this->t('Avif Processor'),
      '#default_value' => $config->get('processor'),
      '#options' => $this->processorsList(),
    ];

    $form['quality'] = [
      '#type' => 'number',
      '#title' => $this->t('Image quality'),
      '#description' => $this->t('Specify the image quality. This setting
       will be in effect for all new image style derivatives. In order to apply
       this setting to existing image style derivatives, flush image styles
       through the interface, or by using Drush or Drupal Console.'),
      '#default_value' => $config->get('quality'),
      '#min' => 1,
      '#max' => 100,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  private function processorsList() {
    $options = ['undefined' => t('Undefined')];
    $type = \Drupal::service('plugin.manager.avif_processor');
    $plugin_definitions = $type->getDefinitions();
    foreach ($plugin_definitions as $pluginKey => $plugin_definition) {
      $options[$pluginKey] = $plugin_definition['label'];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('avif.settings')
      ->set('quality', (int) $form_state->getValue('quality'))
      ->set('processor', (string) $form_state->getValue('processor'))
      ->save();
  }

}
