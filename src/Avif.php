<?php

namespace Drupal\avif;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Provides the Avif utility class.
 *
 * @package Drupal\avif
 */
class Avif {

  use StringTranslationTrait;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Default image processing quality.
   *
   * @var int
   */
  protected $defaultQuality;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Avif conversion processor.
   *
   * @var string
   */
  protected $processor;

  /**
   * Plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * Avif constructor.
   *
   * @param \Drupal\Core\Image\ImageFactory $imageFactory
   *   Image factory to be used.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger channel factory.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $stringTranslation
   *   String translation interface.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration factory.
   * @param \Drupal\Core\File\FileSystem $fileSystem
   *   The file system service.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $pluginManager
   *   The plugin manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   Lock backend interface.
   */
  public function __construct(ImageFactory $imageFactory, LoggerChannelFactoryInterface $loggerFactory, TranslationInterface $stringTranslation, ConfigFactoryInterface $configFactory, FileSystem $fileSystem, ClientInterface $httpClient, PluginManagerInterface $pluginManager, LockBackendInterface $lock) {
    $this->imageFactory = $imageFactory;
    $this->logger = $loggerFactory->get('avif');
    $this->setStringTranslation($stringTranslation);
    $this->defaultQuality = $configFactory->get('avif.settings')->get('quality');
    $this->fileSystem = $fileSystem;
    $this->httpClient = $httpClient;
    $this->processor = $configFactory->get('avif.settings')->get('processor');
    $this->pluginManager = $pluginManager;
    $this->lock = $lock;
  }

  /**
   * Get an Avif copy of a source image URI.
   *
   * @param string $uri
   *   Image URI.
   * @param int $quality
   *   Image quality factor (optional).
   *
   * @return \Drupal\Core\Image\ImageInterface|false
   *   The Avif image if successful, FALSE if not successful.
   */
  public function getAvifCopy($uri, $quality = NULL) {
    $destination = $uri . '.avif';

    // If the avif copy already exists just return it immediately.
    if (file_exists($destination)) {
      return $this->imageFactory->get($destination);
    }

    return $this->createAvifImage($uri, $destination, $quality);
  }

  /**
   * Receives the srcset string of an image and returns the avif equivalent.
   *
   * @param string $srcset
   *   Srcset to convert to .avif version.
   *
   * @return string
   *   Avif version of srcset
   */
  public static function getAvifSrcset($srcset) {
    return preg_replace('/\.(png|jpg|jpeg)(\\?.*?)?(,| |$)/i', '.\\1.avif\\2\\3', $srcset);
  }

  /**
   * Creates a Avif copy of a source image URI using imagemagick toolkit.
   *
   * @param string $uri
   *   Image URI.
   * @param string $destination
   *   Destination URI.
   * @param int $quality
   *   Image quality factor (optional).
   *
   * @return \Drupal\Core\Image\ImageInterface|false
   *   The location of the Avif image if successful, FALSE if not successful.
   */
  protected function createAvifImage($uri, string $destination, $quality = NULL) {
    if ($quality === NULL) {
      $quality = $this->defaultQuality;
    }

    $plugin_definitions = $this->pluginManager->getDefinitions();
    if (!isset($plugin_definitions[$this->processor])) {
      $this->logger->error('The Avif processor is not defined, please check the AVIF Config.', []);
      return FALSE;
    }

    $plugin = $this->pluginManager->createInstance($this->processor);

    // Don't start generating the avif copy if generation is in progress in
    // another thread.
    $lock_name = $this->acquireLock($uri);
    $success = $plugin->convert($uri, $quality, $destination);
    $this->lock->release($lock_name);

    if ($success === FALSE) {
      $this->logger->error('The Avif conversion library returns an error.');
      return FALSE;
    }

    return $this->imageFactory->get($destination);
  }

  /**
   * Acquires a lock.
   *
   * @param string $uri
   *   The URI of the image to be processed.
   *
   * @return string
   *   The name of the lock.
   */
  protected function acquireLock(string $uri): string {
    $lock_name = 'avif_create_copy:' . Crypt::hashBase64($uri);
    $lock_acquired = $this->lock->acquire($lock_name, 3);
    if (!$lock_acquired) {
      // Tell client to retry again in 3 seconds. Currently no browsers are
      // known to support Retry-After.
      throw new ServiceUnavailableHttpException(3, $this->t('Image generation in progress. Try again shortly.'));
    }
    return $lock_name;
  }

}
