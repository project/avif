<?php

namespace Drupal\avif\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Avif processor plugins.
 */
abstract class AvifProcessorBase extends PluginBase implements AvifProcessorInterface {

  /**
   * {@inheritdoc}
   */
  abstract public function convert($image_uri, $quality, $destination);

}
