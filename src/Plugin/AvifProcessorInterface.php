<?php

namespace Drupal\avif\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Avif processor plugins.
 */
interface AvifProcessorInterface extends PluginInspectionInterface {

  /**
   * Convert a source image to AVIF format.
   *
   * @param string $image_uri
   *   The URI of the source image.
   * @param int $quality
   *   The quality (0-100) for the AVIF image, if supported.
   * @param string $destination
   *   The URI of the destination image.
   *
   * @return bool
   *   Returns TRUE on success, otherwise FALSE.
   */
  public function convert($image_uri, $quality, $destination);

}
