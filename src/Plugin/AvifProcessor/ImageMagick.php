<?php

namespace Drupal\avif\Plugin\AvifProcessor;

use Drupal\avif\Plugin\AvifProcessorBase;
use Drupal\Core\Image\ImageFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Uses the Image Magick to convert images.
 *
 * @AvifProcessor(
 *   id = "imagemagick",
 *   label = @Translation("ImageMagick")
 * )
 */
class ImageMagick extends AvifProcessorBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The image factory.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImageFactory $imageFactory, LoggerInterface $loggerFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->imageFactory = $imageFactory;
    $this->logger = $loggerFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('image.factory'),
      $container->get('logger.factory')->get('avif')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function convert($image_uri, $quality, $destination) {
    $avif = FALSE;

    // Check AVIF support.
    $toolkit = $this->imageFactory->getToolkitId();
    if (!in_array('avif', $this->imageFactory->getSupportedExtensions($toolkit))) {
      $error = $this->t("Your image toolkit '%toolkit' doesn't support AVIF format.", ['%toolkit' => $toolkit]);
      $this->logger->error($error);

      return $avif;
    }

    if ($toolkit != 'imagemagick') {
      $error = $this->t("This plugin works only with Image Magick or your toolkit is: %toolkit.", ['%toolkit' => $toolkit]);
      $this->logger->error($error);

      return $avif;
    }

    $imageMagickImage = $this->imageFactory->get($image_uri, 'imagemagick');

    // We'll convert the image into avif.
    $converted = $imageMagickImage->apply('convert', [
      'extension' => 'avif',
      'quality' => $quality,
    ]);

    if ($converted && $imageMagickImage->save($destination)) {
      $avif = $destination;
    }
    else {
      $error = $this->t('Imagemagick issue: Could not generate Avif image.');
      $this->logger->error($error);
    }

    return $avif;
  }

}
