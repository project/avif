<?php

declare(strict_types = 1);

namespace Drupal\avif_test\Plugin\AvifProcessor;

use Drupal\avif\Plugin\AvifProcessorBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Dummy avif processor for tests.
 *
 * @AvifProcessor(
 *   id = "avif_test",
 *   label = @Translation("Avif test")
 * )
 */
class AvifTestProcessor extends AvifProcessorBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Avif test constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected FileSystemInterface $fileSystem, protected LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system'),
      $container->get('logger.channel.avif'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function convert($image_uri, $quality, $destination): bool {
    if ($quality < 0 || $quality > 100) {
      return FALSE;
    }
    $this->logger->debug(sprintf('Converting %s to %s with quality %d', $image_uri, $destination, $quality));
    $this->fileSystem->copy($image_uri, $destination, FileSystemInterface::EXISTS_REPLACE);
    return TRUE;
  }

}
