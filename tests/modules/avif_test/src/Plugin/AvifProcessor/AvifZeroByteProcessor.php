<?php

declare(strict_types = 1);

namespace Drupal\avif_test\Plugin\AvifProcessor;

use Drupal\avif\Plugin\AvifProcessorBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Dummy avif processor for tests that produces zero byte files.
 *
 * @AvifProcessor(
 *   id = "avif_test_zero",
 *   label = @Translation("Avif test zero bytes")
 * )
 */
class AvifZeroByteProcessor extends AvifProcessorBase implements ContainerFactoryPluginInterface {

  /**
   * Avif test constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('file_system'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function convert($image_uri, $quality, $destination): bool {
    $this->fileSystem->saveData('', $destination, FileSystemInterface::EXISTS_REPLACE);
    return TRUE;
  }

}
