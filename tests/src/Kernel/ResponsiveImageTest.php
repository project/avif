<?php

declare(strict_types = 1);

namespace Drupal\Tests\avif\Kernel;

use Drupal\Core\File\FileSystemInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Test responsive image rendering.
 *
 * @group avif
 */
final class ResponsiveImageTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'image',
    'breakpoint',
    'responsive_image',
    'responsive_image_test_module',
    'avif',
  ];

  /**
   * A directory where test image files can be saved to.
   */
  protected string $directory;

  /**
   * The file system servicve.
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The image style.
   */
  protected ImageStyleInterface $imageStyle;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig([
      'system',
      'image',
      'responsive_image',
      'responsive_image_test_module',
    ]);

    // Prepare a directory for test file results.
    $this->directory = 'public://avif';
    $this->fileSystem = $this->container->get('file_system');
    $this->fileSystem->prepareDirectory($this->directory, FileSystemInterface::CREATE_DIRECTORY);
    $this->fileSystem->copy('core/tests/fixtures/files/image-test.jpg', $this->directory . '/image-test.jpg', FileSystemInterface::EXISTS_REPLACE);

    // Set up image styles.
    $this->imageStyle = ImageStyle::create([
      'name' => 'avif',
    ]);
    $this->imageStyle->save();

    ResponsiveImageStyle::create([
      'id' => 'avif',
      'label' => 'AVIF',
      'breakpoint_group' => 'responsive_image_test_module',
      'fallback_image_style' => 'avif',
    ])->addImageStyleMapping('responsive_image_test_module.mobile', '1x', [
      'image_mapping_type' => 'image_style',
      'image_mapping' => 'avif',
    ])->save();
  }

  /**
   * Test responsive image theme.
   *
   * @covers \avif_preprocess_responsive_image
   */
  public function testPreprocess(): void {
    $build = [
      '#theme' => 'responsive_image',
      '#uri' => $this->directory . '/image-test.jpg',
      '#responsive_image_style_id' => $this->imageStyle->id(),
    ];

    $renderer = $this->container->get('renderer');
    $output = $renderer->renderPlain($build);

    $crawler = (new Crawler((string) $output))->filter('picture');
    $avif = $crawler->filter('source[srcset][type="image/avif"]');
    $jpg = $crawler->filter('source[srcset][type="image/jpeg"]');

    $this->assertCount(1, $avif);
    $this->assertCount(1, $jpg);
    $this->assertStringContainsString($this->getImageStyleRealPath('image-test.jpg.avif'), $avif->attr('srcset'));
    $this->assertStringContainsString($this->getImageStyleRealPath('image-test.jpg'), $jpg->attr('srcset'));
  }

  /**
   * Gets the real path of an image style.
   */
  private function getImageStyleRealPath(string $filename): string {
    return $this->fileSystem->realpath(
      $this->imageStyle->buildUri($this->directory . '/' . $filename)
    );
  }

}
