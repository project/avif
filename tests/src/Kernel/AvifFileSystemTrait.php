<?php

declare(strict_types = 1);

namespace Drupal\Tests\avif\Kernel;

use Drupal\Core\Site\Settings;
use Drupal\Core\Test\TestDatabase;

/**
 * Sets up a real file system for testing in kernel tests.
 */
trait AvifFileSystemTrait {

  /**
   * {@inheritdoc}
   */
  protected function setUpFilesystem(): void {
    parent::setUpFilesystem();

    // Create a test-specific but real (no vfs://) files directory.
    $test_db = new TestDatabase($this->databasePrefix);
    $files_directory = $test_db->getTestSitePath() . '/files';
    mkdir($files_directory, 0775, TRUE);

    $settings = Settings::getInstance() ? Settings::getAll() : [];
    $settings['file_public_path'] = $files_directory;
    new Settings($settings);
  }

}
