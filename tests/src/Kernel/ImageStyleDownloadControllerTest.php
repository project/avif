<?php

declare(strict_types = 1);

namespace Drupal\Tests\avif\Kernel;

use Drupal\avif\Controller\ImageStyleDownloadController;
use Drupal\Core\File\FileSystemInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tests for ImageStyleDownloadController.
 *
 * @coversDefaultClass \Drupal\avif\Controller\ImageStyleDownloadController
 * @group avif
 */
final class ImageStyleDownloadControllerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'image',
    'avif',
    'avif_test',
  ];

  /**
   * The image style.
   */
  protected ImageStyleInterface $imageStyle;

  /**
   * The file system.
   */
  protected FileSystemInterface $fileSystem;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'image', 'avif', 'avif_test']);

    // Prepare a directory for test file results.
    $directory = 'public://avif';
    $this->fileSystem = $this->container->get('file_system');
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $this->fileSystem->copy('core/tests/fixtures/files/image-test.jpg', $directory . '/image-test.jpg', FileSystemInterface::EXISTS_REPLACE);

    // Set up image styles.
    $this->imageStyle = ImageStyle::create(['name' => 'avif']);
    $this->imageStyle->save();

    // Set processor.
    $avifConfig = \Drupal::configFactory()->getEditable('avif.settings');
    $avifConfig->set('processor', 'avif_test')->save(TRUE);
  }

  /**
   * @covers ::deliver
   * @dataProvider providerDeliver
   */
  public function testDeliver(string $source, string $destination, string $responseClass): void {
    $request = new Request(
      query: [
        'file' => $destination,
        IMAGE_DERIVATIVE_TOKEN => $this->imageStyle->getPathToken('public://' . $source),
      ],
      server: [
        'HTTP_ACCEPT' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,*/*;q=0.8',
      ],
    );
    $controller = ImageStyleDownloadController::create(\Drupal::getContainer());
    $response = $controller->deliver($request, 'public', $this->imageStyle);
    $this->assertInstanceOf($responseClass, $response);
    $this->assertSame('public://styles/avif/public/' . $destination, $response->getFile()->getPathname());
    $file = $response->getFile();
    $this->assertFileExists($file->getPathname());
    $this->assertGreaterThan(0, $file->getSize());
    $this->assertGreaterThan(0, $response->headers->get('Content-Length'));
  }

  /**
   * Tests that zero-byte files are handled correctly.
   */
  public function testZeroByte(): void {
    // Set processor.
    $avifConfig = \Drupal::configFactory()->getEditable('avif.settings');
    $avifConfig->set('processor', 'avif_test_zero')->save(TRUE);

    $request = new Request(
      query: [
        'file' => 'avif/image-test.jpg.avif',
        IMAGE_DERIVATIVE_TOKEN => $this->imageStyle->getPathToken('public://avif/image-test.jpg'),
      ],
      server: [
        'HTTP_ACCEPT' => 'image/avif',
      ],
    );

    $controller = ImageStyleDownloadController::create(\Drupal::getContainer());
    $response = $controller->deliver($request, 'public', $this->imageStyle);
    $this->assertNotInstanceOf(BinaryFileResponse::class, $response);
    $this->assertSame('Error generating image, unable to generate avif derivative.', $response->getContent());
    $this->assertSame(500, $response->getStatusCode());
  }

  /**
   * Data provider for ::testDeliver.
   */
  public function providerDeliver(): array {
    return [
      'avif' => [
        'avif/image-test.jpg',
        'avif/image-test.jpg.avif',
        BinaryFileResponse::class,
      ],
      'jpg' => [
        'avif/image-test.jpg',
        'avif/image-test.jpg',
        BinaryFileResponse::class,
      ],
    ];
  }

}
