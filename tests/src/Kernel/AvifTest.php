<?php

declare(strict_types = 1);

namespace Drupal\Tests\avif\Kernel;

use Drupal\avif\Avif;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\DatabaseLockBackend;
use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\KernelTests\KernelTestBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\ServiceUnavailableHttpException;

/**
 * Tests the Avif service class.
 *
 * @coversDefaultClass \Drupal\avif\Avif
 * @group avif
 */
class AvifTest extends KernelTestBase implements LoggerInterface {

  use RfcLoggerTrait;
  use AvifFileSystemTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'avif',
    'avif_test',
  ];

  /**
   * The file system.
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Log messages.
   */
  protected array $logMessages = [];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->installConfig(['system', 'avif', 'avif_test']);
    $this->container->get('logger.factory')->addLogger($this);

    // Prepare a directory for test file results.
    $directory = 'public://avif';
    $this->fileSystem = $this->container->get('file_system');
    $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    $this->fileSystem->copy('core/tests/fixtures/files/image-test.jpg', $directory . '/image-test.jpg', FileSystemInterface::EXISTS_REPLACE);

    // Set processor.
    $avifConfig = \Drupal::configFactory()->getEditable('avif.settings');
    $avifConfig->set('processor', 'avif_test')->save(TRUE);
  }

  /**
   * Test creating an avif copy.
   *
   * @covers ::getAvifCopy
   * @covers ::createAvifImage
   */
  public function testGetAvifCopy(): void {
    $uri = 'public://avif/image-test.jpg';
    $destination = 'public://avif/image-test.jpg.avif';
    $quality = 10;

    $avif = $this->container->get('avif.avif');
    $this->assertInstanceOf(Avif::class, $avif);

    $copy = $avif->getAvifCopy($uri, $quality);
    $this->assertFileExists($copy->getSource());
    $this->assertSame($destination, $copy->getSource());

    $log = $this->logMessages[0];
    $this->assertSame(sprintf('Converting %s to %s with quality %d', $uri, $destination, $quality), $log['message']);
  }

  /**
   * Test getting an avif copy that already exists.
   *
   * @covers ::getAvifCopy
   */
  public function testGetAvifCopyExisting(): void {
    $uri = 'public://avif/image-test.jpg';
    $destination = 'public://avif/image-test.jpg.avif';
    $avif = $this->container->get('avif.avif');

    $existing = $this->fileSystem->copy($uri, $destination);
    touch($existing, 1000);
    $this->assertSame(1000, filemtime($existing));

    $copy = $avif->getAvifCopy($uri);
    $this->assertSame(1000, filemtime($copy->getSource()));
  }

  /**
   * Test when a lock cannot be acquired.
   *
   * @covers ::createAvifImage
   */
  public function testCreateAvifImageNoLock(): void {
    $uri = 'public://avif/image-test.jpg';

    // Use a database lock backend so we can populate it.
    $lock = new DatabaseLockBackend($this->container->get('database'));
    $this->container->set('lock', $lock);
    $avif = $this->container->get('avif.avif');

    // Clone the lock service so the static cache is empty in the container.
    $avifLock = clone $lock;
    $avifLock->acquire('avif_create_copy:' . Crypt::hashBase64($uri));

    // Ensure the exception is thrown.
    $this->expectException(ServiceUnavailableHttpException::class);
    $this->expectExceptionMessage('Image generation in progress. Try again shortly.');
    $avif->getAvifCopy($uri);
  }

  /**
   * Test when no processor is configured.
   *
   * @covers ::createAvifImage
   */
  public function testCreateAvifImageNoProcessor(): void {
    $avifConfig = \Drupal::configFactory()->getEditable('avif.settings');
    $avifConfig->delete();

    $avif = $this->container->get('avif.avif');
    $copy = $avif->getAvifCopy('foo', 100);
    $this->assertFalse($copy);

    $log = $this->logMessages[0];
    $this->assertSame('The Avif processor is not defined, please check the AVIF Config.', $log['message']);
  }

  /**
   * Test when convert failed.
   *
   * @covers ::createAvifImage
   */
  public function testCreateAvifImageConvertFailed(): void {
    $uri = 'public://avif/image-test.jpg';

    $avif = $this->container->get('avif.avif');
    $copy = $avif->getAvifCopy($uri, -10);
    $this->assertFalse($copy);

    $log = $this->logMessages[0];
    $this->assertSame('The Avif conversion library returns an error.', $log['message']);
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []): void {
    if (isset($context['channel']) && $context['channel'] === 'avif') {
      $this->logMessages[] = [
        'level' => $level,
        'message' => $message,
        'context' => $context,
      ];
    }
  }

}
