<?php

declare(strict_types = 1);

namespace Drupal\Tests\avif\Kernel;

use Drupal\avif\Controller\ImageStyleDownloadController;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests for RouteSubscriber.
 *
 * @coversDefaultClass \Drupal\avif\Routing\RouteSubscriber
 * @group avif
 */
final class RouteSubscriberTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'image',
    'avif',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig(['system', 'image', 'avif']);
  }

  /**
   * @covers ::alterRoutes
   */
  public function testAlterRoutes(): void {
    /** @var \Drupal\Core\Routing\AccessAwareRouterInterface $router */
    $router = \Drupal::service('router');
    $collection = $router->getRouteCollection();
    $route = $collection->get('image.style_public');
    $this->assertSame(ImageStyleDownloadController::class . '::deliver', $route->getDefault('_controller'));
  }

}
