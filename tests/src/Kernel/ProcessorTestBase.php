<?php

declare(strict_types = 1);

namespace Drupal\Tests\avif\Kernel;

use Drupal\avif\Plugin\AvifProcessorBase;
use Drupal\Core\File\FileSystemInterface;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test bases for AvifProcessor plugins.
 */
abstract class ProcessorTestBase extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
  ];

  /**
   * The image factory service.
   *
   * @var \Drupal\Core\Image\ImageFactory
   */
  protected $imageFactory;

  /**
   * A directory where test image files can be saved to.
   */
  protected string $directory;

  /**
   * The processor.
   */
  protected AvifProcessorBase $processor;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig(['system']);
    $this->imageFactory = $this->container->get('image.factory');

    // Prepare a directory for test file results.
    $file_system = $this->container->get('file_system');
    $this->directory = 'public://avif';
    $file_system->prepareDirectory($this->directory, FileSystemInterface::CREATE_DIRECTORY);
    $file_system->copy('core/tests/fixtures/files/image-test.jpg', $this->directory . '/image-test.jpg', FileSystemInterface::EXISTS_REPLACE);
  }

  /**
   * Test when convert succeeded.
   *
   * @covers ::convert
   */
  public function testConvert(): void {
    $image = $this->imageFactory->get($this->directory . '/image-test.jpg');
    $destination = $this->directory . '/image-test.avif';
    $result = $this->processor->convert($image->getSource(), 100, $destination);
    $this->assertNotFalse($result);
    $this->assertFileExists($destination);
  }

  /**
   * Test when convert failed.
   *
   * @covers ::convert
   */
  public function testConvertFailed(): void {
    $image = $this->imageFactory->get($this->directory . '/image-test.txt');
    $destination = $this->directory . '/image-test.avif';
    $result = $this->processor->convert($image->getSource(), 100, $destination);
    $this->assertFalse($result);
    $this->assertFileDoesNotExist($destination);
  }

}
