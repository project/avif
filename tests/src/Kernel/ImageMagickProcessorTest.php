<?php

declare(strict_types = 1);

namespace Drupal\Tests\avif\Kernel;

use Drupal\avif\Plugin\AvifProcessor\ImageMagick;
use Drupal\Tests\imagemagick\Kernel\ToolkitSetupTrait;

/**
 * Tests the ImageMagickProcessor class.
 *
 * @coversDefaultClass \Drupal\avif\Plugin\AvifProcessor\ImageMagick
 * @group avif
 */
class ImageMagickProcessorTest extends ProcessorTestBase {

  use ToolkitSetupTrait;
  use AvifFileSystemTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file_mdm',
    'sophron',
    'imagemagick',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig(['imagemagick', 'file_mdm', 'sophron']);
    $this->setUpToolkit('imagemagick', 'imagemagick.settings', [
      'binaries' => 'imagemagick',
      'quality' => 100,
      'debug' => TRUE,
      'locale' => 'en_US.UTF-8 C.UTF-8',
    ]);
    $config = \Drupal::configFactory()->getEditable('imagemagick.settings');
    $formats = $config->get('image_formats');
    $formats['AVIF']['enabled'] = TRUE;
    $config->set('image_formats', $formats)->save();
    $this->processor = ImageMagick::create($this->container, [], 'imagemagick', []);
  }

}
