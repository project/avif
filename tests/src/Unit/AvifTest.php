<?php

declare(strict_types = 1);

namespace Drupal\Tests\avif\Unit;

use Drupal\avif\Avif;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for AVIF service.
 *
 * @coversDefaultClass \Drupal\avif\Avif;
 */
class AvifTest extends UnitTestCase {

  /**
   * @covers ::getAvifSrcset
   * @dataProvider providerGetAvifSrcset
   */
  public function testGetAvifSrcset(string $srcset, string $expected): void {
    $this->assertEquals($expected, Avif::getAvifSrcset($srcset));
  }

  /**
   * Data provider for ::testGetAvifSrcset.
   */
  public function providerGetAvifSrcset(): array {
    return [
      'jpg' => [
        '/test-image.jpg',
        '/test-image.jpg.avif',
      ],
      'jpeg' => [
        '/test-image.jpeg',
        '/test-image.jpeg.avif',
      ],
      'png' => [
        '/test-image.png',
        '/test-image.png.avif',
      ],
      'avif' => [
        '/test-image.avif',
        '/test-image.avif',
      ],
      'gif' => [
        '/test-image.gif',
        '/test-image.gif',
      ],
      'jpg with query string' => [
        '/test-image.jpg?itok=1234',
        '/test-image.jpg.avif?itok=1234',
      ],
      'jpg with .avif in filename' => [
        '/test-image.avif.jpg?itok=1234',
        '/test-image.avif.jpg.avif?itok=1234',
      ],
    ];
  }

}
