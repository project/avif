<?php

declare(strict_types = 1);

namespace Drupal\Tests\avif\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the SettingsForm class.
 *
 * @coversDefaultClass \Drupal\avif\Form\SettingsForm
 * @group avif
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'avif',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the settings form.
   *
   * @covers ::buildForm
   * @covers ::submitForm
   */
  public function testSettingsForm(): void {
    $url = Url::fromRoute('avif.settings_form');

    // Anonymous user cannot access.
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(403);

    // Admin user can access.
    $this->drupalLogin($this->drupalCreateUser([
      'administer site configuration',
    ]));
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(200);

    // Test fields.
    $this->submitForm([
      'Avif Processor' => 'imagemagick',
      'Image quality' => 100,
    ], 'Save configuration');

    $this->assertSession()->responseContains('The configuration options have been saved.');
    $this->assertSession()->fieldValueEquals('Avif Processor', 'imagemagick');
    $this->assertSession()->fieldValueEquals('Image quality', 100);
  }

}
